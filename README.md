# webscraping diarios arg

Las páginas webs no están diseñadas para un análisis automático, por eso es necesario revisar cada caso para poder ver la estructura HTML y obtener los datos que se quieran relevar. Para poder generar un corpus de información para ser utilizado en ciencias sociales, diseñamos este scritp qie permite descargar noticias de diarios online a través de la búsqueda usando GOGLE.

Para eso se utilizó la librería de Python newspaper que permite analizar diarios on line para obtener los datos XML del diario y conseguir el texto, resumen, título y autor de la noticia que se está analizando.

De esta forma se consigue dos bases de datos, una con capturas de pantalla completas de los artículos de diario seleccionados, y otra base con el texto de la noticia para poder hacer un procesamiento de conteo de frecuencia de palabras y obtener las palabras de uso más frecuente en todas las notas seleccionadas (eliminando palabras vaciás que no son necesarias para el análisis así como números y símbolos no alfanuméricos). Esta información si bien no hace al proceso de investigación, si puede ser útil para direccionar un proceso de selección de casos de estudio posterior de corte más cualitativo, ademas de conservar una base de datos con capturas de pantallas de páginas webs que pueden desaparecer en  cualquier momento.

Como citar esta herramienta:


Basel, V. (2024). webscraping diarios arg (Version 0.1) [Computer software]. https://gitlab.com/valentinbasel/webscraping-diarios-arg
