from datetime import datetime
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from bs4 import BeautifulSoup
from newspaper import Article
import pandas as pd
import os
from collections import OrderedDict

###############################################################################
# solo tocar las siguientes 4 lineas no comentadas
diario = "www.lavoz.com.ar/"
busqueda = "ni una menos"
leer_todo = False  # las opciones que puede manejar son : True o False
cantidad_busquedas = 10  # solo funciona si leer_todo = False
tiempo_espera = 1  # en segundos
ver_navegador = True
###############################################################################


class BUSCADOR:
    def setup_method(self, method, opc):
        options = Options()
        if opc == False:
            options.add_argument("--headless")

        self.driver = webdriver.Firefox(options=options)
        self.vars = {}

    def teardown_method(self, method):
        self.driver.quit()

    def buscador(self, pagina):
        band = True
        self.driver.get("https://www.google.com/")
        self.driver.set_window_size(911, 686)
        self.driver.switch_to.frame(0)
        self.driver.find_element(By.CSS_SELECTOR, ".rr4y5c").click()
        self.driver.switch_to.default_content()
        self.driver.find_element(By.ID, "APjFqb").click()
        self.driver.find_element(By.ID, "APjFqb").send_keys(pagina)
        self.url = []
        time.sleep(tiempo_espera)
        self.driver.find_element(
            By.CSS_SELECTOR, "center:nth-child(1) > .gNO89b"
        ).click()
        time.sleep(tiempo_espera)

        while band == True:
            html = self.driver.page_source
            soup = BeautifulSoup(html, "html.parser")
            for a in soup.find_all("a", href=True):
                print("Found the URL:", a["href"])
                self.url.append(a["href"])
            try:
                self.driver.execute_script(
                    "window.scrollTo(0,document.body.scrollHeight);"
                )
                time.sleep(tiempo_espera)
                self.driver.find_element(
                    By.CSS_SELECTOR, "#pnnext > span:nth-child(2)"
                ).click()
                # self.driver.find_element(By.CSS_SELECTOR, "div.GNJvt").click()
                time.sleep(tiempo_espera)
            except Exception:
                band = False
        return self.url


datos_para_buscar = "site:" + diario + " " + busqueda
busc = BUSCADOR()
busc.setup_method(None, ver_navegador)
datos_url = busc.buscador(datos_para_buscar)
datos_url = list(OrderedDict.fromkeys(set(datos_url)))
options = Options()
options.add_argument("--headless")
driver = webdriver.Firefox(options=options)
data_diario = []
p = 0
print(datos_url)
for line in datos_url:
    nota_a_buscar = "https://" + diario
    # if not line:
    #    break
    if line.find(nota_a_buscar) == 0:
        p = p + 1
        url = line
        # print(p,"----",url)
        try:
            print("url: ", url)
            driver.get(url)
            driver.set_window_size(1362, 725)
            nombre = url.replace("/", "_")
            nombre = nombre.replace("\n", "")
            nombre = nombre.replace(":", "")
            nombre = nombre.replace(".", "")
            nombre = nombre.replace("-", "_")
            directorio_diario = (
                "img/"  # diario.replace(".","")+busqueda.replace(" ","")
            )
            if not os.path.exists(directorio_diario):
                os.makedirs(directorio_diario)
            archi = directorio_diario + "/" + nombre + ".png"
            driver.get_full_page_screenshot_as_file(archi)

            article = Article(url)
            article.download()
            article.parse()
            print("articulo: ", article.url)
            data_diario.append(
                {
                    "url": url,
                    "fecha": article.publish_date,
                    "autor": article.authors,
                    "top_imagen": article.top_image,
                    "archivo_img": archi,
                    "titulo": article.title,
                    "resumen": article.summary,
                    "articulo": article.text,
                }
            )

        except Exception as e:
            # raise e

            data_diario.append(
                {
                    "url": url,
                    "fecha": "NULL",
                    "autor": "NULL",
                    "top_imagen": "NULL",
                    "archivo_img": "NULL",
                    "titulo": "NULL",
                    "resumen": "NULL",
                    "articulo": "NULL",
                }
            )
    if leer_todo == False:
        if p == cantidad_busquedas:  # and band == True:
            print("salgo del bucle")
            break

df = pd.DataFrame(data_diario)
print(df)
fecha = str(datetime.today())
fecha = fecha.replace(":", "_")
fecha = fecha.replace(".", "_")
fecha = fecha.replace(" ", "__")
fecha = fecha.replace("-", "_")
csv = busqueda.replace(" ", "_") + fecha + ".csv"
print(csv)
df.to_csv(csv)
